#!/bin/bash

function check_presence_key () {
    vm=$1
    local_key_path=$2
    check="$(multipass exec "${vm}" sudo grep "`cat $local_key_path`" /root/.ssh/authorized_keys)"
    
    if [[ "$check" != "" ]]
    then
        echo "1"
        return
    fi
    echo "0"
}

function add_authorized_keys () {
    vm=$1
    local_key_path=$2
    multipass exec -- "${vm}" sudo mkdir -p /root/.ssh/
    multipass exec "${vm}" sudo touch /root/.ssh/authorized_keys

    if [[ "$(check_presence_key ${vm} ${local_key_path})" == "1" ]]
    then
        echo "SSH key ${local_key_path} already present"
    else
        cat $local_key_path | multipass exec "${vm}" -- sudo tee -a /root/.ssh/authorized_keys
    fi
}