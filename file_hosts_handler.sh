#!/bin/bash



function hosts_file_backup () {
    mkdir -p /tmp/mubuntu-vms
    sudo cat /etc/hosts > /tmp/mubuntu-vms/hosts_backup
    echo "Created /etc/hosts backup in /tmp/mubuntu-vms/hosts_backup"
}


function delete_vms_from_hosts_file () {
    echo "Deleting from /etc/hosts lines:"
    grep "# managed by mubuntu-vms" /etc/hosts
    sudo sed -i '' '/# managed by mubuntu-vms/d' /etc/hosts
}

function file_hosts_writer () {
    main_name=$1
    host_names=$2
    vm_ip=$3

    NOTE="# managed by mubuntu-vms"

    echo "Adding to /etc/hosts lines:"
    echo "${vm_ip} ${main_name}${HOSTNAME_PREFIX} ${NOTE}" | sudo tee -a /etc/hosts


    for hostname in $host_names
    do
        echo "${vm_ip} ${hostname}${HOSTNAME_PREFIX} ${NOTE}" | sudo tee -a /etc/hosts
    done
}