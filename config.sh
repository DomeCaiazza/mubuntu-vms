#!/bin/bash

declare -a authorize_key_for_root=(
    "$HOME/.ssh/id_rsa.pub"
    "$HOME/.ssh/id_dsa.pub"
    )

declare -a vms=(
    # vmname->[site/app 1] [site/app 2]
    "vm-dev1->application1 application1-second-name"
    )

HOSTNAME_PREFIX=".mubuntu.local" # for all vms hostname

#VMS info

CPUS=2
DISK="20G"
MEM="2G"
UBUNTU_VERSION="22.04"