# mubuntu-vms

Automate the startup and shutdown of local multiple Ubuntu Servers VM and that modify the /etc/hosts file dynamically with passed hostnames.

It is based on Multipass, it was born for MacOS, but it also works on Linux.

## Compatibility

- [X] Apple Silicon M1 M2
- [X] Intel
- [X] AMD


### Add a VM
in config.sh
```
declare -a vms=(
    "[VM-NAME]->[HOSTNAME 1] [HOSTNAME 2] [HOSTNAME N]"
    #eg for a vm named vm-dev1:
    #"vm-dev1->application1 application1-second-name"
    )
```

In which case the configuration will be automatically written to the /etc/hosts file:

##### example
```
192.168.xx.x vm-dev1.mubuntu.local # managed by mubuntu-vms
192.168.xx.x application1.mubuntu.local # managed by mubuntu-vms
192.168.xx.x application1-second-name.mubuntu.local # managed by mubuntu-vms
```

### Add key for root ssh login
in config.sh
```
declare -a authorize_key_for_root=(
    "$HOME/.ssh/id_rsa.pub"
    "$HOME/.ssh/id_dsa.pub"
    )
```
You can automatically perform ssh access without any further modifications or configurations on the machines.

### Set Ubuntu vms version
in config.sh
```
UBUNTU_VERSION="22.04"
```
Versions: `20.04`, `22.04`


### VMS hardware for vm
in config.sh
```
CPUS="2"
DISK="20G"
MEM="2G"
```


### USAGE
#### Start
`bash start` or `./start`

#### Stop
`bash stop` or `./stop`

